# Toha Theme Plan-K customization

This is a customized [Hugo Toha Theme](https://github.com/hugo-toha/toha) copy that is used to create Plan-K website layouts en styling:

- Removed example content
- Added `go.mod` to reuse it with the Hugo Module system

